BinaryLeo - Leonardo Moura

<img align="right" alt="img" src="https://user-images.githubusercontent.com/72607039/169917184-a7ca071a-46f8-417d-8037-d73beae94358.png" width="40%" height="auto" />
 


![GitHub followers](https://img.shields.io/github/followers/binaryleo?label=Follow&style=social)
<img src="https://komarev.com/ghpvc/?username=binaryleo&color=008080" alt="Profile views " />


Hi there! Welcome to my Github page! I am Leonardo Moura, a Brazilian Front-End and Mobile developer, I am currently learning Kotlin and Swift!

<div>

```javascript
const binaryleo = {
  pronouns: ["He", "Him"],
  title: ["Front-End", "Mobile developer"],
  language: ["PT-BR 🇧🇷", "EN 🇨🇦", "NOR 🇳🇴", "FR 🇫🇷"],
  askMeAbout: ["Mobile development", "Iot", "Trail running"],
};

```
[![My Skills](https://skillicons.dev/icons?i=linux,js,nodejs,ts,react,html,css,cs,c,cpp,py,java,kotlin,swift,dotnet,vite,express,webpack,prisma,materialui,tailwind,styledcomponents,sass,qt,spring,figma,postgres,bash,powershell,androidstudio,idea,visualstudio,vscode,eclipse,arduino,raspberrypi&perline=18)](https://github.com/BinaryLeo)

For many years I have built Cordova hybrid apps as a freelancer. 
It allowed me to enjoy many projects, including for overseas customers. I'm currently working as Front-end | Graphic Designer. 
I'm always pushing to learn new technologies and to stay current, both at work and at home in my personal time. For example, I like to spend time on DIY IoT Projects (C++), Design Windows GUI applications (Pyside2, Windows forms and QT) and study the Norwegian language.


![BinaryLeo ' Activity Graph](https://activity-graph.herokuapp.com/graph?username=BinaryLeo&custom_title=BinaryLeo%20Contribution%20Graph&theme=react-dark&bg_color=0D1117&hide_border=true&line=7490ac&point=d8dee9)
  
</div>

